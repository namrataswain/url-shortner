/** @format */

import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Table from './Table';
import NavBar from './NavBar';
import Layout from './Layout';

const App = () => {
	return (
		<Fragment>
			<NavBar />
			<Router>
				<Route path='/' exact render={(props) => <Table {...props} />} />
				<Route path='/layout' exact render={(props) => <Layout {...props} />} />
				
			</Router>
		</Fragment>
	);
};

export default App;
