/** @format */

import React, { useState } from 'react';
import Axios from 'axios';
import './styles.css';
import { Formik } from 'formik';

export default function App({openGenerateUrl, setOpenGenerateUrl}) {
	
	const [isLoading, setLoading] = useState(false);
	

	const getShortURL = (values) => {
		if (isLoading) return null;
		setLoading(true);
		createShortUrl(getCampaignUrl(values), values.short_URL);
	};

	const createShortUrl = (longUrl, shortUrl) => {
		const apiData = {
			body: {
				long_url: longUrl,
				user_id: '1545112863',
			},
			hosted_event_id: '',
			short_url: shortUrl,
		};
		Axios.post(
			`https://fh0gcvl3wb.execute-api.us-east-2.amazonaws.com/dev/create`,
			apiData
		)
			.then((res) => {
				alert('Your Short Url is Created');
				setLoading(false);
			})
			.catch((err) => {
				alert('OOps! Something went Wrong');
				setLoading(false);
			});
	};

	return (
		<div className='App'>
			<div className={`quickview  ${openGenerateUrl && 'is-active'}`}>
			<header className='quickview-header is-primary'
          style={{ minHeight: '4.3em' }}>
          <p className='title has-text-weight-bold'>Generate Url</p>
          <span
            className='delete is-size-5'
			data-dismiss='quickview'
			onClick={() => setOpenGenerateUrl(false)}
            ></span>
        </header>
			<Formik
				initialValues={{
					campaign_name: '',
					url: '',
					email: '',
					frequency: 0,
					campaign_source: '',
					campaign_medium: '',
					short_URL: '',
					campaign_url: '',
				}}
				onSubmit={(values, { setSubmitting, resetForm }) => {
					// props.addOrganiser(values);
					console.log(values);
					getShortURL(values);
				}}>
				{({
					values,
					errors,
					touched,
					handleChange,
					handleBlur,
					handleSubmit,
					copyCodeToClipboard,
					isSubmitting,
				}) => (
					<section className='section is-mobile'>
						<form onSubmit={handleSubmit}>
							<div className='columns'>
								<div className='column is-full'>
									<div className='control field'>
										<label className='label' htmlFor='url'>
											URL<span className='has-text-danger'>*</span>
										</label>
										<div className='field-body'>
											<div className='field'>
												<p className='control'>
													<input
														className='input'
														type='url'
														id='url'
														disabled={false}
														onChange={handleChange}
														value={values.url}
														placeholder='Campaign url'
														required={true}
													/>
													<div className='help has-text-grey'>
														Include http:// or https// in the URL, like
														https://ccdays.konfhub.com
													</div>
												</p>
											</div>
										</div>
									</div>

									<div className='control field'>
										<label className='label' htmlFor='campaign_name'>
											Campaign Name
										</label>

										<div className='field-body'>
											<div className='field'>
												<div className='control'>
													<input
														className='input'
														type='text'
														id='campaign_name'
														value={values.campaign_name}
														onChange={handleChange}
														onBlur={handleBlur}
														placeholder='Campaign name'
													/>
													<div className='help has-text-grey'>
														Product, promo code, or slogan (e.g. paid_promotion,
														outreach)
													</div>
												</div>
											</div>
										</div>
									</div>

									<div className='control field'>
										<label className='label' htmlFor='campaign_source'>
											Campaign Source
										</label>
										<div className='field-body'>
											<div className='field'>
												<div className='control'>
													<input
														className='input'
														id='campaign_source'
														value={values.campaign_source}
														type='text'
														onChange={handleChange}
														placeholder='Campaign Source'
													/>
												</div>
											</div>
										</div>
									</div>

									<div className=' control field'>
										<label className='label' htmlFor='campaign_medium'>
											Campaign Medium
										</label>
										<div className='field-body'>
											<div className='field'>
												<div className='control'>
													<input
														className='input'
														type='text'
														id='campaign_medium'
														value={values.campaign_medium}
														onChange={handleChange}
														onBlur={handleBlur}
														placeholder='Campaign Medium'
													/>
												</div>
											</div>
										</div>
									</div>
								</div>

									
								
							</div>
							<div className='columns'>
								<div className='column is-9'>
									<div className='control field'>
										<label className='label' htmlFor='url'>
											Campaign URL
										</label>
										<div className='field-body'>
											<div className='field'>
												<div className='control is-flex'>
													<textarea
														className='input'
														id='campaign_url'
														disabled={true}
														onChange={handleChange}
														value={getCampaignUrl(values)}
														placeholder='Campaign url'
														required={true}
													/>{' '}
													<span
														className='fa fa-copy is-size-5 has-text-success'
														style={{ margin: '3px', cursor: 'copy' }}
														onClick={() => {
															navigator.clipboard.writeText(
																getCampaignUrl(values)
															);
														}}
													/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className='control'>
								<label className='label'>Short URL
								<span className='has-text-danger'>*</span>
								</label>
								<div className='field has-addons'>
									<p className='control'>
										<button className='button is-static'>
											https://konf.me/
										</button>
									</p>
									<p className='control is-flex'>
										<input
											className='input'
											type='text'
											placeholder='Short Name'
											onChange={handleChange}
											name='short_URL'
											value={values.short_URL}
											required={true}
										/>
										<span
											className='fa fa-copy is-size-5 has-text-success'
											style={{ margin: '3px', cursor: 'copy' }}
											onClick={() => {
												navigator.clipboard.writeText('lalit');
											}}
										/>
									</p>
								</div>
							</div>
							<br />
							<div className='field is-grouped'>
								<p className='control'>
									<button
										type='submit'
										className={`button is-info ${isLoading && 'is-loading'}`}
										disabled={isLoading}>
										Generate Short URL
									</button>
								</p>

								
							</div>
						</form>
					</section>
				)}
			</Formik>
			</div>
		</div>
	);
}
const getCampaignUrl = (_data) => {
	let _url = _data.url + '?';
	if (_data.campaign_source.trim() !== '') {
		_url = _url + 'utm_source=' + encodeURIComponent(_data.campaign_source);
	}
	if (_data.campaign_medium.trim() !== '') {
		_url =
			_url +
			(_url.indexOf('utm_source') > -1 ? '&' : '') +
			'utm_medium=' +
			encodeURIComponent(_data.campaign_medium);
	}
	if (_data.campaign_name.trim() !== '') {
		_url =
			_url +
			(_url.indexOf('utm_source') > -1
				? '&'
				: _url.indexOf('utm_medium') > -1
				? '&'
				: '') +
			'utm_campaign=' +
			encodeURIComponent(_data.campaign_name);
	}
	_url = _url === _data.url + '?' ? _data.url : _url;
	return _url;
};
