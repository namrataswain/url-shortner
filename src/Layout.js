import React, { Fragment, useState} from 'react';
import Table from './Table';
import Entry from './Entry';


const Layout = () => {
    const[urlDetails, setUrlDetails] = useState(null);
    console.log(urlDetails);
    return(
        <Fragment>
            <div className='columns'>
                <div className='column is-one-third'>
                 <Table setUrlDetails= {setUrlDetails}/>
                </div>
                <div className='column is-two-thirds'>
                {urlDetails !== null && <Entry urlDetails={urlDetails}/>}
                </div>
            </div>

        </Fragment>
    )

}

export default Layout;