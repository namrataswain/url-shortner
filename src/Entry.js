import React, { useState } from 'react';
import moment from 'moment';
import GenerateUrl from './GenrateUrl';


const Entry = ({ urlDetails }) => {
    const [openGenerateUrl, setOpenGenerateUrl] = useState(false);
    
    return (
        <div>
            <section className="Section"
                style={{ paddingTop: '100px', paddingRight: '20px' }}
            >
                <div class="card">
                    <div class="card-content">
                        <time>
                            Created {moment(urlDetails.created_at).format(' MMMM Do YYYY')}
                        </time>

                        <p class="title">
                            {urlDetails.shortened_url}
                        </p>
                        <p class="subtitle">

                            <a href={urlDetails.longer_url} target="_blank" rel="noopener noreferrer">
                                {urlDetails.longer_url}
                            </a>
                            <span class="icon is-size-5 has-text-success">
                                <i class="fas fa-external-link-alt "></i>
                            </span>

                        </p>
                        <div className='buttons'>
                            <button className="button is-rounded is-info">Copy</button>
                            <button className="button is-rounded is-info"
                                onClick={() => setOpenGenerateUrl(true)}
                            >Edit</button>
                            <GenerateUrl openGenerateUrl={openGenerateUrl} setOpenGenerateUrl={setOpenGenerateUrl} />
                            <button className="button is-rounded is-info"
                            >Notification</button>
                        </div>
                    </div>
                </div>
            </section>

        </div>

    )
}
export default Entry;