/** @format */

import React, { useState } from 'react';
import GenerateUrl from './GenrateUrl';
const NavBar = (props) => {
	const [show, setShow] = useState(' ');
	const [openGenerateUrl, setOpenGenerateUrl] = useState(false);
	return (
		<div>
			<nav
				className='navbar is-primary '
				role='navigation'
				aria-label='main navigation'>
				<div className='navbar-brand' style={{ minHeight: '4rem' }}>
					<a className='navbar-item' href={window.location.origin}>
						<img
							src='https://konfhub.com/images/logo.svg'
							width='100'
							height='38'
							alt='konfhub'
						/>
					</a>

					<span
						role='button'
						className='navbar-burger burger'
						aria-label='menu'
						aria-expanded='false'
						data-target='navbarBasicExample'
						onClick={(e) => {
							setShow(() => {
								return show === ' ' ? ' is-active' : ' ';
							});
						}}>
						<span aria-hidden='true'></span>
						<span aria-hidden='true'></span>
						<span aria-hidden='true'></span>
					</span>
				</div>

				<div id='navbarBasicExample' className={'navbar-menu' + show}>
					<div className='navbar-end'>
						<a className='navbar-item' onClick={() => setOpenGenerateUrl(true)}
						data-show="quickview" data-target="quickviewDefault" href='/'>
							Generate
						</a>
					</div>
				</div>
			</nav>
			<GenerateUrl openGenerateUrl={openGenerateUrl} setOpenGenerateUrl={setOpenGenerateUrl}/>
					</div>
	);
};

export default NavBar;
