/** @format */

import React from 'react';
import Axios from 'axios';
import moment from 'moment';


export default class Table extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			links: [
				{
					id: '',
					longer_url: '',
					shortened_url: ' ',
					hit_count: '',
					created_at: '',
				},
			],
			originalLink: [
				{
					id: '',
					longer_url: '',
					shortened_url: ' ',
					hit_count: '',
					created_at: '',
				},
			],
			isLoading: false,
			OpenEntryUrl: false,
		};
	}

	componentDidMount() {
		this.setState({
			isLoading: true,
		});
		Axios.get(
			'https://fh0gcvl3wb.execute-api.us-east-2.amazonaws.com/dev/analytics?user_id=1545112863',
			{
				responseType: 'json',
			}
		).then((response) => {
			this.setState({
				links: response.data.body,
				originalLink: response.data.body,
				isLoading: false,
			});
		});
	}

	handleChange = (event) => {
		const tempLinks = this.state.originalLink.filter((item) => {
			return (
				item.shortened_url.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
				item.longer_url.toLowerCase().indexOf(event.toLowerCase()) > -1
			);
		});

		this.setState({ links: tempLinks });
	};

	showEntry = (event) => {
		this.setState({ OpenEntryUrl: true });
	}

	render() {
		if (!this.state.isLoading) {
			return (
				<article class="panel is-primary">
					<section className='section'>
						<div className='level'>
							<input
								type='text'
								className='input'
								placeholder='Search...'
								onChange={(e) => this.handleChange(e.target.value)}
							/>

							<p>
								<button className='button is-primary'>
									Total URLs generated: {this.state.links.length}
								</button>
							</p>
						</div>



						{this.state.links.map((item) => {
							return (
								<div className='card-footer-border-top' key={item.created_at}>
									<div className='card' onClick={() => this.props.setUrlDetails(item)}>

										<div className='card-content'>
											<span className='subtitle' 
											>
												<span className='tag is-primary is-medium' 
												style={{marginLeft : '260px' }}>
													Hits : {item.hit_count} 
														</span>
											</span>
                                            <br/>
											<div className='media-content'>
												<p className='title is-4'>
													{item.shortened_url}{' '}
												</p>
											</div>

											<div
												className='content has-text-weight-bold'
												style={{ overflowX: 'auto' }}>
												<span className='is-size-7'>{item.longer_url}</span>
												<br />

												<time>
													{moment(item.created_at).format(' MMMM Do YYYY')}
												</time>
												<br />
											</div>
										</div>
									</div>
								</div>
							);
						})}
					</section>
				</article>

			);
		} else {
			return (
				<div className='pageloader'>
					<span className='title'>Pageloader</span>
				</div>
			);
		}
	}
}
